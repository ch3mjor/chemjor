### Collins Chemjor

Kenya, Nairobi area with a preference for remote work.

### Objective

My primary objective in life is to have an impact... on my team, my community, and the world. My goal is to do this by offering software and infrastructure solutions to real-world issues while also creating and using Free and Open Source Software.

### About Me

I am a humble, collaborative, ambitious, and continuously learning Software and DevOps engineer. I have been delivering large-scale distributed solutions in Nairobi for the last 3 years. In that time I have developed a wide range of experience which includes being an individual contributor, system administrator, lead and as a software engineer.

I am passionate about DevOps, Python and creating amazing code.

I am familiar with multiple cloud infrastructure providers, including Amazon Web Services and Microsoft Azure, as well as multiple container orchestration frameworks including Kubernetes and Docker. I am well versed in Agile practices, Lean Product Development, and Infrastructure as Code.

I am pragmatic in both my work style and technology choices, seeing the world as a series of trade-offs that must be carefully considered and occasionally reevaluated. I have worked fully remote for over one and a half years, am an excellent communicator, and highly effective at asynchronous collaboration. I value flexibility, autonomy and deep focus enabling a state of flow. I love Linux, Git and opportunities to solve new puzzles in a variety of languages.

### Experience

#### April 2020-Present | Software Engineer | Power (https://m-power.io)

- Performed end to end software development life cycles (SDLC) projects and activities.
- Deployed code while using caching tools such as Redis.
- Carried out job queuing using Celery.
- Supported core infrastructure services and developer tooling
- Produced technical specifications from design documents.
- Worked on Azure and Digital Ocean to deploy solutions involving cloud firewalls, servers and load balancers.

#### May 2019- April 2020 | Software Developer | Telkom Kenya (https://www.telkom.co.ke/)

- Assisted organisation to grow T-KASH revenue by 5% through reducing system downtime.
- Hands on experience deploying the T-KASH mobile money platform, as well as carrying out support on production.
- Daily use of Git, CI/CD (Jenkins), Docker and Kubernetes.

#### September 2017-December 2017 | Quality Assurance | KABARAK UNIVERSITY (https://kabarak.ac.ke/)

- Developing and executing manual test cases for the University Applications

- Assisted in setting up the School of Health networking infrastructure

- Identify, clearly document and prioritize defects.

#### Past Roles

- September 2016-December-2016 | Senior Clerk | Kenya National Examinations Council (https://www.knec.ac.ke/)
- September 2015-December-2015 | Senior Clerk | Kenya National Examinations Council (https://www.knec.ac.ke/)

### Key Skills

- Disciplines: Agile, DevOps, Infrastructure Engineering
- Languages: Bash, HTML, Python, SQL
- Cloud Platforms: Amazon Web Services, Microsoft Azure
- Orchestrators: Kubernetes, Docker
- Observability: Nagios
- Continuous Integration: Jenkins

### Recent Training

- Bachelor in Software Engineering (2018)
